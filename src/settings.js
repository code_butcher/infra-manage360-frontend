const DEBUG = true;
let BASE_URL = '';

if(DEBUG){
    BASE_URL = "http://localhost:8188/api"
}else{
    BASE_URL = "http://" + window.location.host + "/api"
}

export {
    DEBUG,
    BASE_URL
}