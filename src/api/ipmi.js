import http from '@/utils/http'

export function addIpmi(ipmi_server_ipaddress, user, password, desc) {
    return http.axiosInstance({
        method: "POST",
        url: "/ipmi-servers",
        data: {
            ipmi_server_ipaddress: ipmi_server_ipaddress,
            user: user,
            password: password,
            desc: desc
        }
    })
}

export function deleteIpmi(r_id) {
    return http.axiosInstance({
        method: "DELETE",
        url: "/ipmi-servers/" + r_id,
    })
}

export function updateIpmi(r_id, user, password, desc) {
    return http.axiosInstance({
        method: "PATCH",
        url: "/ipmi-servers/" + r_id,
        data: {
            user: user,
            password: password,
            desc: desc
        }
    })
}

export function ipmiList(page=1, page_size=10) {
    return http.axiosInstance({
        method: "GET",
        url: "/ipmi-servers",
        params: {
            page, page_size
        }
    })
}

export function getIpmi(r_id) {
    return http.axiosInstance({
        method: "GET",
        url: "/ipmi-servers/" + r_id,
    })
}

export function startMachine(r_id) {
    return http.axiosInstance({
        method: "GET",
        url: "/ipmi-servers/" + r_id + "/power-on",
    })
}

export function shutdownMachine(r_id) {
    return http.axiosInstance({
        method: "GET",
        url: "/ipmi-servers/" + r_id + "/power-soft",
    })
}