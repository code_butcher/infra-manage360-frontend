import http from '@/utils/http'

export function login(name, password) {
    return http.axiosInstance({
        method: "POST",
        url: "/user/login",
        data: {
            name: name,
            password: password
        }
    })
}

export function register(name, password) {
    return http.axiosInstance({
        method: "POST",
        url: "/user/register",
        data: {
            name: name,
            password: password
        }
    })
}

export function logout() {
    return http.axiosInstance({
        url: "/user/logout"
    })
}

export function info() {
    return http.axiosInstance({
        url: "/user/info"
    })
}